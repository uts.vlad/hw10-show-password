const passwordEnterField = document.getElementById('password-field-enter');
const passwordConfirmField = document.getElementById('password-field-confirm');
const submit = document.querySelector("button[type='submit']");

const enterPasFieldIcon = document.querySelector('#pas-f-enter-icon');
const confirmPasFieldIcon = document.querySelector('#pas-f-confirm-icon');

const throwInputError =()=>{
    alert('Input Error!  Re-fill corresponding password fields');
    clearFields()
};

const clearFields=()=>{
    passwordEnterField.value = "";
    passwordEnterField.setAttribute( 'type', 'password');
        passwordConfirmField.value = "";
    passwordConfirmField.setAttribute( 'type', 'password');

    enterPasFieldIcon.classList.toggle("fa-eye", true);
    enterPasFieldIcon.classList.toggle("fa-eye-slash", false);
        confirmPasFieldIcon.classList.toggle('fa-eye', true);
    confirmPasFieldIcon.classList.toggle('fa-eye-slash', false);
};

const toggleFieldIcon =(el)=>{
    el.target.classList.toggle('fa-eye');
    el.target.classList.toggle('fa-eye-slash');
    const currentInput = el.target.closest('label').children[0];
    if (currentInput.getAttribute('type') === 'password') currentInput.setAttribute('type', 'text');
    else currentInput.setAttribute('type', 'password');
};


const comparePasswords =(ev)=>{
    ev.preventDefault();
    if (passwordEnterField.value !== passwordConfirmField.value) throwInputError();
    else { alert('You are welcome!'); clearFields()}
};


enterPasFieldIcon.addEventListener('click', toggleFieldIcon);
confirmPasFieldIcon.addEventListener('click', toggleFieldIcon);
submit.addEventListener('click', comparePasswords);

